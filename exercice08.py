import string
def get_word_count(sentence):
       # Votre code ici
       flag = False
       liste=[]
       count=0
       if (len(sentence)==0):
              return 0
       sentence = sentence.lower()
       for lettre in string.ascii_lowercase:
              liste.append(lettre)
       
       if (sentence[0] in liste):
              count+=1
       for lettre in sentence:
              if (flag == True):
                     if (lettre in liste):
                            count+=1
                            flag = False
              elif (lettre == " "):
                     flag = True
              else:
                     pass
       return count

def run():
   assert get_word_count("Bonjour") == 1
   assert get_word_count("Bonjour toi") == 2
   assert get_word_count("Bonjour ca va ?") == 3
   assert get_word_count("Bonjour ca va toi ?!") == 4
   assert get_word_count("") == 0

run()
