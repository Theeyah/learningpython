import string
def get_letter_count(word):
       # Votre code ici
       count=0
       list_of_letter=[]
       word = word.lower()
       for letter in string.ascii_lowercase:
              list_of_letter.append(letter)
       for letter in word:
              if (letter in list_of_letter):
                     count +=1
       return count

def run():
   assert get_letter_count("Oui") == 3
   assert get_letter_count("Bonjour") == 7
   assert get_letter_count("") == 0
   assert get_letter_count(".........hein???") == 4
   assert get_letter_count("Attention y'a quatre mots !") == 21

